const { Sequelize } = require('sequelize')

const createConnection = () => {
  const database = new Sequelize(process.env.DATABASE_URL, { logging: false })
  return {
    get: () => database
  }
}

const connection = createConnection()

module.exports = {
  sequelize: connection.get
}
