module.exports = {
  eoloplantRouter: require('./eoloplant'),
  websocketsRouter: require('./websocket')
}
