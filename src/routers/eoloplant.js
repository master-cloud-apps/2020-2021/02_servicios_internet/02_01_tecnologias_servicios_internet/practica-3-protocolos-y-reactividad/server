const express = require('express')
const { eoloplants } = require('./../endpoints')

const routerHandler = ({ eventEmitter }) => {
  const router = new express.Router()
  const eolplantHanlder = eoloplants({ eventEmitter })

  router.post('/api/eoloplants', eolplantHanlder.post)
  router.get('/api/eoloplants', eolplantHanlder.getEoloplants)
  router.get('/api/eoloplants/:cityId', eolplantHanlder.getCityById)
  router.delete('/api/eoloplants/:cityId', eolplantHanlder.deleteEoloplantById)

  return router
}

module.exports = routerHandler
