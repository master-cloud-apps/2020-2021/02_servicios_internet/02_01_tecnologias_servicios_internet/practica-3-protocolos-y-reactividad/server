/* global utils */

// eslint-disable-next-line no-unused-vars
const dom = (function () {
  const appendEoloplantInDom = plant => {
    const el = document.createElement('li')
    el.innerHTML = utils.getEoloplantDescription(plant)
    document.querySelector('#eoloplants').appendChild(el)
  }

  function processPlants (eoloplants) {
    document.querySelector('#eoloplants').innerHTML = '<ul id="eoloplants"></ul>'
    eoloplants.forEach(appendEoloplantInDom)
  }

  const hideForm = (cityStatus) => {
    document.querySelector('#createPlantForm').style.visibility = 'hidden'
    document.querySelector('#eoloplantStatus').innerHTML = cityStatus
  }

  const updateFormAndDisplayNewPlant = (eoloplantData) => {
    document.querySelector('#createPlantForm').style.visibility = 'visible'
    document.querySelector('#createPlantForm').reset()
    document.querySelector('#eoloplantStatus').innerHTML = ''
    if (eoloplantData.progress && eoloplantData.city) {
      dom.appendEoloplantInDom(eoloplantData)
    }
  }

  const button = document.querySelector('button')
  button.addEventListener('click', () => {
    const city = document.querySelector('#city').value
    utils.createPlant(city)
  })
  utils.getEoloplants()
    .then(data => processPlants(data))

  return {
    appendEoloplantInDom,
    processPlants,
    hideForm,
    updateFormAndDisplayNewPlant
  }
})()
