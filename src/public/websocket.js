/* global utils,dom */

(function () {
  const socket = new WebSocket(`ws://${window.location.host}/notifications`)

  socket.onopen = function (e) {
    socket.send('userId')
    console.log('WebSocket connection established')
  }

  socket.onmessage = function (event) {
    const data = JSON.parse(event.data)
    console.log(data.message)
    console.log(`[kind] Message kind: ${data.kind}`)
    if (data.message.userId && data.kind === 'login') {
      utils.setUserId(data.message.userId)
    }
    console.log(`[message] Data received from server: ${event.data}`)
    if (data.message.completed === false) {
      dom.hideForm(utils.getEoloplantDescription(data.message))
    } else {
      dom.updateFormAndDisplayNewPlant(data.message)
      utils.getEoloplants()
        .then(responseData => dom.processPlants(responseData))
    }
  }
})()
