const express = require('express')
const path = require('path')
const EventEmitter = require('events')
const { eoloplantRouter, websocketsRouter } = require('./../routers')

const createApp = ({ eventEmitter = new EventEmitter(), websockets = {} }) => {
  const app = express()
  require('express-ws')(app)

  app.use(express.json())
  app.use(express.static(path.join(__dirname, '..', 'public')))
  app.disable('x-powered-by')

  app.use(eoloplantRouter({ eventEmitter, websockets }))
  app.use(websocketsRouter({ eventEmitter, websockets }))

  return app
}

module.exports = {
  createApp, app: createApp({})
}
