const { DataTypes } = require('sequelize')
const { sequelize } = require('../db/sequelize')

const Eoloplant = sequelize().define('Eoloplant', {
  userId: {
    type: DataTypes.STRING,
    allowNull: false
  },
  city: {
    type: DataTypes.STRING,
    allowNull: false
  },
  progress: {
    type: DataTypes.DECIMAL,
    allowNull: false
  },
  completed: {
    type: DataTypes.BOOLEAN
  },
  planning: {
    type: DataTypes.STRING
  }
}, {})

module.exports = {
  Eoloplant
}
