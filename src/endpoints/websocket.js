const uuid = require('node-uuid')
const { Eoloplant } = require('../models/eoloplant')

const handlers = ({ eventEmitter, websockets }) => {
  eventEmitter.on('plant_progress', (plantProgress) => {
    Eoloplant.findByPk(parseInt(JSON.parse(plantProgress).id))
      .then(dbPlant => {
        if (websockets[dbPlant.userId]) {
          websockets[dbPlant.userId].send(JSON.stringify({
            message: dbPlant,
            kind: 'plant_progress'
          }))
        }
      })
  })
  eventEmitter.on('plant_finished', (plantFinished) => {
    console.log('plant finished')
    Eoloplant.findByPk(parseInt(JSON.parse(plantFinished).id))
      .then(dbPlant => {
        for (const socket in websockets) {
          if (socket !== dbPlant.userId) {
            console.log(socket)
            websockets[socket].send(JSON.stringify({
              message: dbPlant,
              kind: 'plant_finished'
            }))
          }
        }
      })
  })
  return {
    registerUser: (ws, req) => {
      ws.id = uuid.v4()
      ws.send(JSON.stringify({ message: { userId: ws.id }, kind: 'login' }))
      websockets[ws.id] = ws
      ws.on('close', () => {
        delete websockets[ws.id]
      })
    }
  }
}

module.exports = handlers
