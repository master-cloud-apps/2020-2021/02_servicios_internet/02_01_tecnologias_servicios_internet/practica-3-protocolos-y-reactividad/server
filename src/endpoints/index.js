const eoloplants = require('./eoloplants')
const websocket = require('./websocket')

module.exports = {
  eoloplants,
  websocket
}
