const amqp = require('amqplib')
const { Eoloplant } = require('../models/eoloplant')
const CHANNEL_CREATION_REQUEST = 'eoloplantCreationRequests'
const CHANNEL_PLANT_PROGRESS = 'eoloplantCreationProgressNotifications'

const handlers = ({ channel, eventEmitter }) => {
  const notifyPlantCreation = data => {
    console.log('Notifying rabbitMQ')
    return channel.assertQueue(CHANNEL_CREATION_REQUEST, { durable: false })
      .then(() => {
        channel.sendToQueue(CHANNEL_CREATION_REQUEST, Buffer.from(data))
      })
      .then(() => ({ status: 'ok' }))
  }
  const consumePlantCreation = () => {
    return channel.assertQueue(CHANNEL_PLANT_PROGRESS, { durable: false })
      .then(() => {
        console.log('Consuming rabbitMQ')
        return channel.consume(CHANNEL_PLANT_PROGRESS, msg => {
          if (msg !== null) {
            channel.ack(msg)
            const eoloplant = JSON.parse(msg.content.toString())
            Eoloplant.update({
              progress: eoloplant.progress,
              completed: eoloplant.completed,
              planning: eoloplant.planning
            }, { where: { id: eoloplant.id } })
              .then(() => {
                console.log(`[${new Date().toISOString()}] Processing ${msg.content.toString()}`)
                eventEmitter.emit('plant_progress', msg.content.toString())
                if (eoloplant.completed) {
                  eventEmitter.emit('plant_finished', msg.content.toString())
                }
              })
              .catch(error => console.log(error))
          } else {
            console.log('Message error')
            eventEmitter.emit('queue_error', 'Error consuming')
          }
        })
      })
  }

  eventEmitter.on('plant_creation', (eoloplant) => {
    notifyPlantCreation(eoloplant)
  })
  return {
    notifyPlantCreation,
    consumePlantCreation
  }
}

const createChannel = (uri) => {
  return amqp.connect(uri)
    .then(conn => conn.createChannel()
      .then(channel => ({ conn, channel })))
}

const closeConnection = (connection) => {
  return connection.close()
}

module.exports = {
  handlers, createChannel, closeConnection
}
