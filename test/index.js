const { sequelize } = require('../src/db/sequelize')

const manageInMemoryDatabase = () => {
  beforeEach(() => sequelize().sync({ force: true }))

  afterEach(() => sequelize().drop())
}

module.exports = {
  manageInMemoryDatabase
}
