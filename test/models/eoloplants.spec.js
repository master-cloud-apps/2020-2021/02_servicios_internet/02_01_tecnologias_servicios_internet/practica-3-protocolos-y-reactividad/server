const { expect } = require('chai')
const { Eoloplant } = require('../../src/models/eoloplant')
const { manageInMemoryDatabase } = require('./../index.js')

describe('Database use cases', () => {
  manageInMemoryDatabase()

  it('Should create element in database', async () => {
    const madridPlant = await Eoloplant.create({ city: 'Madrid', progress: 0, completed: false, planning: null, userId: 1234 })

    expect(madridPlant.id).to.be.equal(1)
  })
  it('Should get all elements in the dabatase', async () => {
    await Eoloplant.create({ city: 'Madrid', progress: 0, completed: false, planning: null, userId: 1234 })
    await Eoloplant.create({ city: 'Madrid', progress: 0, completed: false, planning: null, userId: 1234 })

    const eoloplants = await Eoloplant.findAll()

    expect(eoloplants).to.be.an('Array')
    expect(eoloplants.length).to.be.equal(2)
  })
})
