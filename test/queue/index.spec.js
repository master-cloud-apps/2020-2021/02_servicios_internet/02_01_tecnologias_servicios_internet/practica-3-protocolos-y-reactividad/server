const { expect } = require('chai')
const { handlers } = require('./../../src/queue/index.js')
const sinon = require('sinon')
const EventEmitter = require('events')
const { manageInMemoryDatabase } = require('../index.js')
const { Eoloplant } = require('../../src/models/eoloplant.js')

describe('AMQP test', () => {
  const channelAPI = {
    assertQueue: (queueName) => Promise.resolve(),
    sendToQueue: sinon.spy(),
    consume: (_, fn) => { fn({ content: Buffer.from(JSON.stringify({ city: 'Madrid', id: 1 })) }) },
    ack: sinon.spy()
  }
  const eventEmitter = new EventEmitter()
  const queueHandler = handlers({ channel: channelAPI, eventEmitter })

  describe('Producer', () => {
    it('Should create message', () => {
      return queueHandler.notifyPlantCreation(JSON.stringify({ id: 2, city: 'Pontevedra' }))
        .then(status => {
          expect(status.status).to.be.equal('ok')
          expect(channelAPI.sendToQueue.calledOnce).to.be.equal(true)
        })
    })
  })
  describe('Consumer', () => {
    manageInMemoryDatabase()
    beforeEach(() => Eoloplant.create({ city: 'Madrid', progress: 100, completed: false, planning: null, userId: 'user1' }))
    it('Should consume a message', (done) => {
      const spyPlantProgress = sinon.spy()
      eventEmitter.on('plant_progress', spyPlantProgress)
      queueHandler.consumePlantCreation({ queue: 'eoloplantCreationRequests' })
        .then(() => setTimeout(() => {
          expect(spyPlantProgress.calledOnce).to.be.equal(true)
          done()
        }, 0))
    })
    it('Should consume a message with plant finished', (done) => {
      channelAPI.consume = (_, fn) => { fn({ content: Buffer.from(JSON.stringify({ city: 'Madrid', id: 1, progress: 100, completed: true })) }) }
      const spyPlantFinished = sinon.spy()
      eventEmitter.on('plant_finished', spyPlantFinished)
      const spyPlantProgress = sinon.spy()
      eventEmitter.on('plant_progress', spyPlantProgress)
      queueHandler.consumePlantCreation({ queue: 'eoloplantCreationRequests' })
        .then(() => setTimeout(() => {
          expect(spyPlantProgress.calledOnce).to.be.equal(true)
          expect(spyPlantFinished.calledOnce).to.be.equal(true)
          done()
        }, 100))
    })
    it('Should consume a message with error', () => {
      channelAPI.consume = (_, fn) => { fn(null) }
      const spyQueueError = sinon.spy()
      eventEmitter.on('queue_error', spyQueueError)
      return queueHandler.consumePlantCreation({ queue: 'eoloplantCreationRequests' })
        .then(() => expect(spyQueueError.calledOnce).to.be.equal(true))
    })
  })
})

describe('Create eoloplant use case', () => {
  const channelAPI = {
    assertQueue: (queueName) => Promise.resolve(),
    sendToQueue: sinon.spy(),
    consume: (_, fn) => { fn({ content: Buffer.from(JSON.stringify({ city: 'Madrid' })) }) },
    ack: sinon.spy()
  }
  let eventEmitter
  beforeEach(() => {
    eventEmitter = new EventEmitter()
    handlers({ channel: channelAPI, eventEmitter })
  })

  it('Given plant created, should notify to the queue', (done) => {
    eventEmitter.emit('plant_creation', JSON.stringify({ id: 1, city: 'Madrid' }))

    setTimeout(() => {
      expect(channelAPI.sendToQueue.calledOnce).to.be.equal(true)
      done()
    }, 100)
  })
})
