const { expect } = require('chai')
const { app } = require('./../../../src/app/index.js')
const { createEoloplant } = require('../eoloplants.js')
const { manageInMemoryDatabase } = require('../../index.js')
const eoloplantHanlder = require('./../../../src/endpoints/eoloplants')
const EventEmitter = require('events')
const sinon = require('sinon')

describe('Eoloplants use cases', () => {
  manageInMemoryDatabase()
  it('When create eoloplant should return ok response', () => {
    return createEoloplant({ eoloplant: { city: 'Madrid' }, app })
      .then(response => {
        expect(response.statusCode).to.be.equal(200)
      })
  })
  it('When create eoloplant Madrid should return new city Madrid response', () => {
    return createEoloplant({ eoloplant: { city: 'Madrid' }, app })
      .then(response => expect(response.body).to.deep.include({ city: 'Madrid', progress: 0 }))
  })
  it('When create eoloplant Berlin should return new city Berlin response', () => {
    return createEoloplant({ eoloplant: { city: 'Berlin' }, app })
      .then(response => expect(response.body).to.deep.include({ city: 'Berlin', progress: 0 }))
  })
  it('When create eoloplant should notify queue', () => {
    const req = {
      body: {
        city: 'Madrid'
      },
      header: (headerName) => 1234
    }
    const res = {
      status: (code) => {},
      send: (data) => {}
    }
    const resMock = sinon.mock(res)
    resMock.expects('status').returnsThis()
    resMock.expects('send').returns({
      statusCode: 200,
      body: {
        city: 'Madrid',
        progress: 0
      }
    })

    const spyPlantProgress = sinon.spy()
    const spyPlantCreation = sinon.spy()
    const eventEmitter = new EventEmitter()
    eventEmitter.on('plant_progress', spyPlantProgress)
    eventEmitter.on('plant_creation', spyPlantCreation)
    const eoloplant = eoloplantHanlder({ eventEmitter })

    return eoloplant.post(req, res)
      .then(response => {
        expect(response.statusCode).to.be.equal(200)
        expect(response.body).to.deep.include({ city: 'Madrid', progress: 0 })
      })
      .then(() => {
        expect(spyPlantProgress.calledOnce).to.be.equal(true)
        expect(spyPlantCreation.calledOnce).to.be.equal(true)
      })
  })
  describe('Create eoloplant errors', () => {
    it('When create eoloplant whitout city should return 400', () => {
      return createEoloplant({ eoloplant: {}, app })
        .then(response => expect(response.statusCode).to.be.equal(400))
    })
    it('When create eoloplant whitout body should return 400', () => {
      return createEoloplant({ eoloplant: undefined, app })
        .then(response => expect(response.statusCode).to.be.equal(400))
    })
    it('When create eoloplant whitout body should return error message for city', () => {
      return createEoloplant({ eoloplant: undefined, app })
        .then(response =>
          expect(response.body.errors[0].message).to.be.equal('Eoloplant.city cannot be null'))
    })
  })
})
