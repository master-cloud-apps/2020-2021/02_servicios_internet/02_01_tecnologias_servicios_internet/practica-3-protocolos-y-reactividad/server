const { expect } = require('chai')
const request = require('supertest')
const { app } = require('../../../src/app/index.js')
const { manageInMemoryDatabase } = require('../../index.js')
const { createEoloplant } = require('../eoloplants.js')

describe('Eoloplants use cases', () => {
  manageInMemoryDatabase()
  it('When no eoloplant created, get eoloplant by id should return 404', () => {
    return request(app)
      .get('/api/eoloplants/1')
      .then(response => {
        expect(response.statusCode).to.be.equal(404)
      })
  })
  it('When one eoloplant created, get eoloplant by id should return 200', () => {
    return createEoloplant({ eoloplant: { city: 'Madrid' }, app })
      .then(response => request(app).get(`/api/eoloplants/${response.body.id}`))
      .then(response => {
        expect(response.statusCode).to.be.equal(200)
      })
  })
  it('Give one eoloplant created, when get all eoloplants should return one element', () => {
    return createEoloplant({ eoloplant: { city: 'Madrid' }, app })
      .then(_ => request(app).get('/api/eoloplants'))
      .then(response => {
        expect(response.statusCode).to.be.equal(200)
        expect(response.body.length).to.be.equal(1)
      })
  })
})
