const { manageInMemoryDatabase } = require('../../index.js')
const { app } = require('./../../../src/app/index.js')
const { createEoloplant } = require('../eoloplants.js')
const request = require('supertest')
const { expect } = require('chai')

describe('Eoloplants deletion use cases', () => {
  manageInMemoryDatabase()
  it('Given created eoloplant when delete eoloplant should return 204', () => {
    return createEoloplant({ eoloplant: { city: 'Moscu' }, app })
      .then(response => response.body.id)
      .then(id => request(app).delete(`/api/eoloplants/${id}`))
      .then(response => expect(response.statusCode).to.be.equal(204))
  })
  it('Given deleted eoloplant when get plant by id should return 404', () => {
    return createEoloplant({ eoloplant: { city: 'Moscu' }, app })
      .then(response => response.body.id)
      .then(id => request(app).delete(`/api/eoloplants/${id}`)
        .then(() => request(app).get(`/api/eoloplants/${id}`)))
      .then(response => expect(response.statusCode).to.be.equal(404))
  })
  it('When deleting eoloplant that does not exist should return 404', () => {
    return request(app).delete('/api/eoloplants/100')
      .then(response => expect(response.statusCode).to.be.equal(404))
  })
})
