const request = require('supertest')

const createEoloplant = ({ eoloplant, app }) => request(app).post('/api/eoloplants').send(eoloplant).set({ userId: 1234 })

module.exports = {
  createEoloplant
}
