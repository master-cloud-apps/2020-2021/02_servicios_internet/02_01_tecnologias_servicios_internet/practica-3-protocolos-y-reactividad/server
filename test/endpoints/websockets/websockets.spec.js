const request = require('superwstest')
const { app } = require('./../../../src/app/index')
const websocketHanlder = require('./../../../src/endpoints/websocket')
const EventEmitter = require('events')
const { expect } = require('chai')
const { Eoloplant } = require('../../../src/models/eoloplant')
const { manageInMemoryDatabase } = require('./../../index.js')

describe('Websockets user cases', () => {
  let server
  beforeEach(() => {
    server = app.listen(0)
  })
  afterEach(() => server.close())
  it('Should get a message from the socket', () => {
    return request.default(server)
      .ws('/notifications')
      .expectText(/userId/)
      .close().expectClosed()
  })
  it('Should get a message from the socket with correct kind', () => {
    return request.default(server)
      .ws('/notifications')
      .expectText(/login/)
      .close().expectClosed()
  })
})

describe('Handle new plant_creation event', () => {
  manageInMemoryDatabase()
  const websockets = (() => {
    let user1Calls = 0
    let user2Calls = 0
    return {
      user1: {
        send: () => {
          console.log('Calling user1')
          user1Calls++
        },
        getCalls: () => user1Calls
      },
      user2: {
        send: () => {
          user2Calls++
        },
        getCalls: () => user2Calls
      }
    }
  })()
  const eventEmitter = new EventEmitter()
  websocketHanlder({ eventEmitter, websockets })
  let testPlant

  beforeEach(async () => {
    testPlant = await Eoloplant.create({ city: 'Madrid', progress: 100, completed: false, planning: null, userId: 'user1' })
  })

  it('Should listen to plant_creation event', (done) => {
    eventEmitter.emit('plant_finished', JSON.stringify(testPlant))
    setTimeout(() => {
      expect(websockets.user2.getCalls()).to.be.equals(1)
      expect(websockets.user1.getCalls()).to.be.equals(0)
      done()
    }, 100)
  })
})
